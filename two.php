- Class is object representation, which can contain properties and methods:

class Something {
  // property declaration
  public $prop = '';

  // method declaration
  public function getProp()
  {
     return $this->prop;
  }

  public function setProp($val)
  {
      $this->prop = $val;
  }
}

- Object is an instance of a class.

$newSomething = new Something;

- Classes can be extended and inherited it's methods an properties.

class ExtendSomething extends Something {
  public $prop = '';

  // Redefine the parent method
  function setProp($new_val)
  {
      $this->prop = parent::getProp(). $new_val;
  }
}

- Overloading allows to dynamically create properties and methods and are used when we interracting with the class,
containing those predefined properties and methods.

class Something {
  // Location for overloaded data.
  private $data = array();

  public function __set($name, $value)
  {
      echo "Setting '$name' to '$value'\n";
      $this->data[$name] = $value;
  }
}

$obj = new Something;

// We are using __set method with the $name as 'a' and $value '1'
$obj->a = 1;

- Singletons are used when we need set a limit of instances from resource consuming classes, like db connection or internet connection.

class Singleton {
  public static function getInstance()
  {
    if (self::$instance == null)
    {
      self::$instance = new Singleton();
    }
 
    // Method returns existing singleton instance or create new one
    return self::$instance;
  }
}