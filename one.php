<?php
function readCsv() 
{
  $fileHandle = fopen("res/source data.csv", "r");
  
  $labels = [];
  $rows = [];
  while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE)
  {
    if (strtolower($row[0]) === "order number")
    {
      $labels = array_map( 'strtolower', $row );

      continue;
    }

    $labeled_row = array_combine($labels, $row);
    $rows = array_merge($rows, [$labeled_row]);
  }
  
  return $rows;
}

$eol = "\r\n";

echo "Starting. <br>";

$order_line = "order number,order date,customer number,item number,quantity,total".$eol;
$customer_line = "customer number,first name,family name".$eol;
$item_line = "item number,description,cost".$eol;

$order_ids = $customer_ids = $item_ids = [];
foreach (readCsv() as $row) 
{
  $is_new_customer = false;
  $is_new_item = false;
  
  foreach ($row as $key => $value) 
  {

    if (in_array($key, ["order number", "order date", "customer number", "item number", "quantity", "total"], true) )
    {
      if ($key == "order number" && !in_array($value, $order_ids))
      {
        array_push($order_ids, $value);
      }

      $order_line .= (substr($customer_line, -2) !== $eol) ? ",".$value : $value;
    }

    if (in_array($key, ["customer number", "first name", "family name"], true) )
    {
      if ($key == "customer number" && !in_array($value, $customer_ids))
      {
        array_push($customer_ids, $value);

        $is_new_customer = true;
      }

      if ($is_new_customer)
      {
        $customer_line .= (substr($customer_line, -2) !== $eol) ? ",".$value : $value;
      }
    }

    if (in_array($key, ["item number", "description", "cost"], true) )
    {
      if ($key == "item number" && !in_array($value, $item_ids))
      {
        array_push($item_ids, $value);

        $is_new_item = true;
      }

      if ($is_new_item)
      {
        $item_line .= (substr($item_line, -2) !== $eol) ? ",".$value : $value;
      }
    }
  }

  if (substr($order_line, -2) !== $eol)
  {
    $order_line .= $eol;
  }
  if (substr($customer_line, -2) !== $eol)
  {
    $customer_line .= $eol;
  }
  if (substr($item_line, -2) !== $eol)
  {
    $item_line .= $eol;
  }
}

file_put_contents('_orders.csv', $order_line, FILE_APPEND | LOCK_EX);
file_put_contents('_customers.csv', $customer_line, FILE_APPEND | LOCK_EX);
file_put_contents('_items.csv', $item_line, FILE_APPEND | LOCK_EX);

echo "Done.<br>";
