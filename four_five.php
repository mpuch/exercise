 <!DOCTYPE html>
<html>
  <head>
    <title>Something</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style>
      body {background-color: gray; text-align: center; position: relative; height: 900px;}
      h1   {color: white; text-align: center;}
      .box {width: 100%; text-align: center; position: absolute;}
      .img_box {position: relative;}
      .text_img_left {
        text-align: right;
        font-size: 18px;
        color: white;
        position: absolute;
        top: 0px;
        padding: 5px 8px 5px 8px;
      }
      .text_box {
        background-color: black;
        width: 100%;
        opacity: 0.7;
        position: absolute;
        bottom: 0px;
      }
      .text_img_right {
        text-align: right;
        font-size: 32px;
        color: white;
        opacity: unset;
        padding: 5px 8px 5px 8px;
      }
      img {
        width: 100%;
      }
      input, textarea {
        width: 200px;
        padding: 5px;
        margin: 5px;
      }
      button {
        width: 200px;
        padding: 5px;
        margin: 5px 0px;
        background-color: green;
        border: none;
        color: white;
        font-weight: bold;
      }
      .hidden {
        display: none;
        margin: auto;
      }
      .error {
        background-color: red;
        color: white;
        margin: auto;
        width: 400px;
      }
      
      @media only screen and (max-width: 600px) {
        body {
          height: 600px;
        }
        .text_img_right {
          text-align: right;
          font-size: 24px;
          color: white;
          opacity: unset;
          padding: 5px 8px 5px 8px;
        }
      }
    </style>
  </head>
  <body>
    <div class="box">
      <div class="img_box">
        <img src="res/thumbnail_ST-SummerNights_1025x400" alt="SummerNights">
        <div class="text_img_left">www.suffolk-holidays.co.uk</div>
        <div class="text_box">
            <div class="text_img_right">Want to buy this domain?</div>
        </div>
      </div>
      <h1>Want to buy this domain?</h1>
      <div>
          <input type="text" id="title" name="title" placeholder="Title">
      </div>
      <div>
          <input type="text" id="surname" name="surname" placeholder="Surname">
      </div>
      <div>
          <input type="text" id="email" name="email" placeholder="Email">
          <div id="emailId" class="hidden">
              <div class="error">Please enter your email address.</div>
          </div>
      </div>
      <div>
          <textarea name="msg" id="msg" placeholder="Message"></textarea>
      </div>
      <div>
          <button type="button">Enquire now</button>
      </div>
    </div>

    <script>
      $("button").on( "click", function( event ) {
        if ($("#email").val() === '') {
          $("#emailId").show();
        } else {
          $.ajax({
            url: "./res/four_five_response.php",
            method: "post",
            data: {
              title: $("#title").val(),
              surname: $("#surname").val(),
              email: $("#email").val(),
              msg: $("#msg").val(),
            },
            success: function( result ) {
              alert(result);
            }
          });
        }
      });
    </script>
  </body>
</html>
  