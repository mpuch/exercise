<?php
function factorial($n) {
  if ($n < 1) return 1;
  $result = 1;
  for ($i=1; $i<=$n; $i++) {
    $result *= $i;
  }
  
  return $result;
}

echo factorial(6);